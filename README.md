# Assignment 4

All solutions must be placed on [CodeAnywhere](https://codeanywhere.com) into the folder `a04`.
Put each problem into the corresponding file `p1.py`, `p2.py`, `p3.py` *etc*.

All files must produce *no errors* when invoked with Python *e.g.* as
```
python p1.py
```




### Problem 1 (*20%*)
Write a function `reverse_string(s)` which takes a string parameter `s` and
returns the string reading backwards (*i.e.* `'jealous'` → `'suolaej'`).
You should *not* use any built-in functions.
Which loop type are you going to use?




### Problem 2 (*80%*)
Remember our class example on parsing? Remind, email headers may look like so:

    'From: Linus Torvalds <torvalds@uq.edu.au> Mon, 12 Dec 2005 17:46:21 +1000 (AEST)'

Write a function `email_addr(s)` that, given a string `s` extracts the email address from it
(in this case `'torvalds@uq.edu.au'`), capitalizes the username (`'Torvalds'`) and converts
the domain name (`'uq.edu.au'`) to uppercase, and returns the result:

    'Torvalds@UQ.EDU.AU'

The above string is just an example, you can be given any email header with any person's name and address and date.
You are guaranteed though that there will be the angle brackets `< ... >` and the at-sign `@`
